import csv
import sys

inputFile1 = sys.argv[1]
inputFile2 = sys.argv[2]

fileSet1 = set()
fileSet2 = set()

with open(inputFile1, 'r') as f:
     reader = csv.reader(f, delimiter=',')
     for row in reader:
          fileSet1.add(row[0])

with open(inputFile2, 'r') as f:
     reader = csv.reader(f, delimiter=',')
     for row in reader:
          fileSet2.add(row[0])
     
intersection = fileSet1.intersection(fileSet2)

print(len(intersection))
