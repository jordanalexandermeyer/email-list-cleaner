import sys
import pandas as pd

inputFile1 = sys.argv[1]
numberOfRows1 = sys.argv[2]
inputFile2 = sys.argv[3]
numberOfRows2 = sys.argv[4]

df1 = pd.read_csv(inputFile1)
shuffledDf1 = df1.sample(frac=1)
cutDf1 = shuffledDf1.iloc[:int(numberOfRows1)]
df2 = pd.read_csv(inputFile2)
shuffledDf2 = df2.sample(frac=1)
cutDf2 = shuffledDf2.iloc[:int(numberOfRows2)]

df3 = pd.concat([cutDf1, cutDf2])

shuffledDf3 = df3.sample(frac=1)

shuffledDf3.to_csv('shuffledFile.csv', index=False)