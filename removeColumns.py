import sys
import pandas as pd

inputFile = sys.argv[1]

df = pd.read_csv(inputFile)

droppedDf = df.drop(columns=['Subscribed Date', 'Subscribe IP', 'Unsubscribe Time', 'Unsubscribe IP', 'Status', 'Email Lists'])

droppedDf.to_csv(inputFile, index=False)