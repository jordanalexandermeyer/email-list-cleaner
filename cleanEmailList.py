import csv
import sys

inputFile = sys.argv[1]

names = [
     'all@',
     'everyone@',
     'ftp@',
     'hostmaster@',
     'investorrelations@',
     'jobs@',
     'marketing@',
     'media@',
     'noc@',
     'noreply@', 
     'no-reply@',
     'postmaster@',
     'prime@',
     'privacy@',
     'remove@',
     'request@',
     'root@',
     'sales@',
     'security@',
     'uce@',
     'usenet@',
     'uucp@',
     'webmaster@',
     'www@',
     '@yahoogroups.com',
     'abuse@',
     'admin@',
     'listadmin@', 
     'serviceadmin@', 
     'interest@',
     'listserv@',
     'esponder@',
     'spam@',
     'subscribe@',
     'users@',
     'info@',
     'news@',
     'office@',
     'support@'
]

isClean = True
cleanIt = False

rowsToKeep = []

with open(inputFile, 'r') as f:
     reader = csv.reader(f, delimiter=',')
     for row in reader:
          keep = True
          for name in names:
               if row[0].startswith(name) or row[0].endswith(name):
                    print(row[0] + " is in " + inputFile)
                    isClean = False
                    keep = False
                    break
          if keep:
               rowsToKeep.append(row)
     if isClean:
          print("It's already clean!")
     else:           
          cleanIt = input("Clean it? y/n: ")
          if cleanIt == "y":
               cleanIt = True
          else:
               cleanIt = False
               print("Did not clean " + inputFile)

if cleanIt and not isClean:
     with open(inputFile, 'w') as writeFile:
          writer = csv.writer(writeFile)
          writer.writerows(rowsToKeep)
     print("Cleaned!")
